module FunctionCollection;

import std.stdio;
import Function;
import Module;
import Number;
import Text;
import Type;
import core.stdc.stdlib;

class FunctionCollection
{
    /// Represents the type 'Function'
    public Type umbraFunction;

    /// Represents the type 'Module'
    public Type umbraModule;

    /// Represents the type 'Number'
    public Type umbraNumber;

    /// Represents the type 'Text'
    public Type umbraText;
    // =================== Standard Umbra Functions ===================

    public Function getFunctionByName(string name, Type* t)
    {
        switch (name)
        {
        case "Font":
            return new Function(name, umbraText, &(this.Font), t);
        case "Size":
            return new Function(name, umbraNumber, &(this.Size), t);
        case "Color":
            return new Function(name, umbraText, &(this.Color), t);
        case "Value":
            return new Function(name, umbraText, &(this.Value), t);
        case "Resolution":
            return new Function(name, umbraText, &(this.Resolution), t);
        case "File":
            return new Function(name, umbraText, &(this.File), t);
        case "Opacity":
            return new Function(name, umbraText, &(this.Opacity), t);
        case "cm":
            return new Function(name, umbraNumber, &(this.cm), t);
        case "mm":
            return new Function(name, umbraNumber, &(this.mm), t);
        case "Above":
            return new Function(name, umbraModule, &(this.Above), t);
        case "Height":
            return new Function(name, null, &(this.Height), t);
        case "Width":
            return new Function(name, null, &(this.Width), t);
        case "Margin":
            return new Function(name, null, &(this.Margin), t);
        default:
            writeln("Function " ~ name ~ " does not exist!");
            exit(-1);
        }
        return null;
    }

    private void Above(Type* t)
    {
    }

    private void Font(Type* t)
    {
    }

    private void Size(Type* t)
    {
    }

    private void Color(Type* t)
    {
    }

    private void Value(Type* t)
    {
    }

    private void Resolution(Type* t)
    {
    }

    private void File(Type* t)
    {
    }

    private void Opacity(Type* t)
    {
    }

    private void cm(Type* t)
    {
    }

    private void mm(Type* t)
    {
    }

    private void Height(Type* t)
    {
    }

    private void Width(Type* t)
    {
    }

    private void Margin(Type* t)
    {
    }

    public bool analyse(string name)
    {
        switch (name)
        {
        case "Height":
            return false;
        case "Width":
            return false;
        case "Margin":
            return false;
        default:
            return true;
        }
    }

    this()
    {
        umbraFunction = new Type("Function");
        umbraModule = new Type("Module");
        umbraNumber = new Type("Number");
        umbraText = new Type("Text");
    }
}
