import Type;

class Image : Type
{
    private string resolution;
    private string file;
    private float opacity;

    this()
    {
        super("Image");
    }

    public string getResolution()
    {
        return (this.resolution);
    }

    public string getFile()
    {
        return (this.file);
    }

    public float getOpacity()
    {
        return (this.opacity);
    }
}
