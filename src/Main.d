import Lexer;
import Parser;
import std.stdio;
import core.stdc.stdlib;

void main(string[] args)
{
    if (args.length < 2)
    {
        writeln("Not enough arguments!");
        exit(2);
    }

    Lexer l = new Lexer(args[1]);
    Parser p = new Parser(l);
}
