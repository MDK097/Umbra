import Type;

class Number : Type
{
    private float value;

    this(float t_value)
    {
        super("Number");
        this.value = t_value;
    }

    public float getNumber()
    {
        return (this.value);
    }
}
