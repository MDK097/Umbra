class Type
{
    private string name;

    this(string t_name)
    {
        this.name = t_name;
    }

    public string getName()
    {
        return (this.name);
    }
}
