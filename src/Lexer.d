module Lexer;

import std.file;
import std.stdio;
import std.string;
import std.ascii;
import core.stdc.stdlib;

class Lexer
{
    private File file;
    private string token;
    private bool is_string_literal;
    private uint line_number;

    this(string filename)
    {
        this.file = File(filename, "r");
        this.is_string_literal = false;
        this.line_number = 1;
    }

    ~this()
    {
        this.file.close();
    }

    public string nextToken()
    {
        this.token = "";
        bool is_alpha_num = false;
        bool is_number = false;

        while (!this.file.eof())
        {
            char c;
            this.file.readf("%c", c);

            if ((this.is_string_literal && c == ' ') || (this.is_string_literal && c == '\t') || (c != ' ' && c != '\t' && c != '\n' && c != '\r'))
            {
                if (!isAlphaNum(c) && ['(', ')', '{', '}', '.', '"', ':', ';', ' ', '\t'].count(c) == 0)
                {
                    if (this.file.eof())
                    {
                        return this.token;
                    }
                    else
                    {
                        stderr.writefln("Invalid token: %c (%d)", c, cast(int)c);
                        exit(-1);
                    }
                }

                if (!this.is_string_literal)
                {
                    if (this.token == "" && isAlphaNum(c))
                    {
                        is_alpha_num = true;

                        if (isDigit(c))
                        {
                            is_number = true;
                        }
                    }
                    else if (this.token != "" && is_alpha_num && !isAlphaNum(c))
                    {
                        if (c == '"')
                        {
                            this.is_string_literal = !this.is_string_literal;
                        }

                        if (!is_number || c != '.')
                        {
                            this.file.seek(-1, SEEK_CUR);
                            return this.token;
                        }
                    }
                    else if (this.token != "" && !is_alpha_num && isAlphaNum(c))
                    {
                        this.file.seek(-1, SEEK_CUR);
                        return this.token;
                    }
                    else if (this.token != "" && !is_alpha_num && !isAlphaNum(c))
                    {
                        if (c == '"')
                        {
                            this.is_string_literal = !this.is_string_literal;
                        }
                        this.file.seek(-1, SEEK_CUR);
                        return this.token;
                    }
                }
                else
                {
                    if (this.token == "\"")
                    {
                        this.file.seek(-1, SEEK_CUR);
                        return this.token;
                    }
                    else if (this.token != "" && c == '"')
                    {
                        this.is_string_literal = !this.is_string_literal;
                        this.file.seek(-1, SEEK_CUR);
                        return this.token;
                    }
                }

                this.token ~= c;
            }
            else
            {
                if (c == '\n')
                {
                    this.line_number++;
                }

                if (chomp(strip(this.token)) != "")
                {
                    return this.token;
                }
                else
                {
                    continue;
                }
            }
        }
        return this.token;
    }

    public void goBack()
    {
        this.file.seek(- this.token.length, SEEK_CUR);
    }

    public uint getLineNumber()
    {
        return this.line_number;
    }

    public bool tokenIsName()
    {
        foreach (char c; this.token)
        {
            if (!isAlpha(c))
            {
                return false;
            }
        }
        return true;
    }

    public bool tokenIsNumber()
    {
        foreach (char c; this.token)
        {
            if (!isDigit(c) && c != '.')
            {
                return false;
            }
        }
        return true;
    }
}
