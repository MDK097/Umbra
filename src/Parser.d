import Lexer;
import Module;
import Type;
import Image;
import Text;
import Number;
import SymbolTable;
import Function;
import FunctionCollection;
import std.stdio;
import core.stdc.stdlib;
import std.conv;

class Parser
{
    private Lexer lexer;
    private string current_token;
    private string current_module;
    private string current_function;
    private FunctionCollection function_collection;
    private SymbolTable symbol_table;

    this(Lexer l)
    {
        this.lexer = l;
        this.function_collection = new FunctionCollection();
        this.symbol_table = new SymbolTable();
        this.start();
    }

    public void error()
    {

        exit(3);
    }

    public void moduleDefinition()
    {

        string module_name;
        Type module_type;

        this.current_token = this.lexer.nextToken();

        if (this.lexer.tokenIsName())
        {
            module_name = this.current_token;
        }
        else
        {
            this.error();
        }

        this.current_token = this.lexer.nextToken();

        if (this.current_token != ":")
        {
            this.error();
        }

        this.current_token = this.lexer.nextToken();

        if (this.current_token == "Image")
        {
            module_type = new Image();
        }
        else if (this.current_token == "Text")
        {
            module_type = new Text("");
        }
        else
        {
            this.error();
        }

        this.current_module = module_name;
        Module m = new Module(module_name, module_type);
        this.symbol_table.insert(m);

        this.current_token = this.lexer.nextToken();

        if (this.current_token != "{")
        {
            this.error();
        }

        while (this.current_token != "}")
        {
            this.attributeFunctionCall();

            if (this.current_token != ";")
            {
                if (this.current_token != "}")
                {
                    this.error();
                }
            }
        }
    }

    public void cardDeclaration()
    {

        this.current_module = this.current_token;
        Module m = new Module(this.current_module, null);
        this.symbol_table.insert(m);
        this.current_token = this.lexer.nextToken();

        if (this.current_token != "{")
        {
            this.error();
        }

        while (this.current_token != "}")
        {
            this.current_token = this.lexer.nextToken();
            if (this.current_token != "this")
            {
                this.moduleFunctionCall();
            }
            else
            {
                this.lexer.goBack();
                this.attributeFunctionCall();
            }

            if (this.current_token != ";")
            {
                if (this.current_token != "}")
                {
                    this.error();
                }
                this.symbol_table.genHTML();
            }
        }
    }

    public void moduleFunctionCall()
    {

        if (this.current_token == "}")
        {
            return;
        }

        string module_name;

        if (!this.lexer.tokenIsName())
        {
            this.error();
        }

        module_name = this.current_token;
        this.current_module = module_name;

        this.current_token = this.lexer.nextToken();

        if (this.current_token != ".")
        {
            this.error();
        }

        this.current_token = this.lexer.nextToken();
        this.functionCall();

    }

    public void attributeFunctionCall()
    {

        this.current_token = this.lexer.nextToken();

        if (this.current_token == "}")
        {
            return;
        }

        if (this.current_token == "this")
        {
            this.current_token = this.lexer.nextToken();

            if (this.current_token != ".")
            {
                this.error();
            }
            else
            {
                this.current_token = this.lexer.nextToken();
            }
        }

        this.functionCall();
    }

    public void functionCall()
    {

        string function_name;

        if (!this.lexer.tokenIsName())
        {
            this.error();
        }

        function_name = this.current_token;
        this.current_function = function_name;

        this.current_token = this.lexer.nextToken();

        if (this.current_token != "(")
        {
            this.error();
        }

        this.current_token = this.lexer.nextToken();

        this.parameter(function_name);

        if (this.current_token != ")")
        {
            this.current_token = this.lexer.nextToken();
        }

        if (this.current_token != ")")
        {
            this.error();
        }

        this.current_token = this.lexer.nextToken();

        if (this.current_token == ":")
        {
            this.specialFunctionCall();
        }
        else
        {
            if (!this.function_collection.analyse(this.current_function))
            {
                this.error();
            }
        }
    }

    public void parameter(string name)
    {

        if (this.current_token == "\"")
        {
            this.text(name);
        }
        else if (this.lexer.tokenIsNumber())
        {
            this.number(name);
        }
        else
        {
            if (this.current_token == ")")
            {
                Type t = new Type("");
                Function f = this.function_collection.getFunctionByName(name, &t);
                this.symbol_table.insert(this.current_module, f);
                return;
            }
            else if (!this.lexer.tokenIsName())
            {
                this.error();
            }

            this.moduleName(name);
        }
    }

    public void specialFunctionCall()
    {
        this.current_token = this.lexer.nextToken();
        this.functionCall();
    }

    public void text(string name)
    {
        this.current_token = this.lexer.nextToken();
        Text t = new Text(this.current_token);
        Function f = this.function_collection.getFunctionByName(name, cast(Type*)(&t));
        this.symbol_table.insert(this.current_module, f);
        this.current_token = this.lexer.nextToken();

        if (this.current_token != "\"")
        {
            this.error();
        }
    }

    public void number(string name)
    {

        if (!this.lexer.tokenIsNumber())
        {
            this.error();
        }

        Number n = new Number(this.current_token.to!float());
        Function f = this.function_collection.getFunctionByName(name, cast(Type*)(&n));
        this.symbol_table.insert(this.current_module, f);
    }

    public void moduleName(string name)
    {

        if (!this.lexer.tokenIsName())
        {
            this.error();
        }

        Function f;
        Module m = this.symbol_table.getModuleByName(this.current_token);
        if (m !is null)
        {
            f = this.function_collection.getFunctionByName(name, cast(Type*)(&m));
            this.symbol_table.insert(this.current_module, f);
        }
    }

    public void start()
    {

        do
        {
            this.current_token = this.lexer.nextToken();

            if (this.current_token == "def")
            {
                this.moduleDefinition();
            }
            else if (this.current_token == "Card")
            {
                this.cardDeclaration();
            }
            else if (this.current_token == "")
            {
                return;
            }
            else
            {
                this.error();
            }

        }
        while (this.current_token != "");
    }
}
