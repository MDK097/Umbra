import std.array;
import std.stdio;
import Function;
import Module;
import std.container : SList;

class SymbolTable
{
    private Function[][Module] table;
    private string[] html_divs;
    private SList!string tag_stack;

    public Module getModuleByName(string name)
    {
        foreach (Module m, Function[] f; this.table)
        {
            if (m.getModuleName() == name)
            {
                return m;
            }
        }
        return null;
    }

    public void insert(string module_name, Function f)
    {
        foreach (Module m, Function[] _f; this.table)
        {
            if (m.getModuleName == module_name)
            {
                this.table[m].insertInPlace(_f.length, f);
            }
        }
    }

    public void insert(Module m)
    {
        this.table[m] = [];
    }

    public void genHTML()
    {
        foreach (Module m, Function[] f; this.table)
        {
            writef("Module %s [", m.getModuleName());
            if (m.getAttachedType() !is null)
            {
                writefln("%s]:\n{", m.getAttachedType().getName());
            }
            else
            {
                writeln("]:\n{");
            }

            foreach (Function func; f)
            {
                writef("    function %s (", func.getFuncName());

                if (func.getParameterType() !is null)
                {
                    writefln("%s)", func.getParameterType().getName());
                }
                else
                {
                    writeln(")");
                }
            }
            writeln("}\n");
        }
    }
}
