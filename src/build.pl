#!/usr/bin/perl

use Term::ANSIColor;
use Capture::Tiny ':all';

chomp(my $total = `ls -1 *.d | wc -l`);
$total++;
my $index = 1;
my @files = split(/\n/, `ls -1 *.d | sort`);

my $percent;
foreach my $i (@files)
{
    print('[');
    $percent = int(($index/$total)*100);
    if ($percent < 10)
    {
        print("  $percent");
    }
    elsif ($percent < 100)
    {
        print(" $percent");
    }
    print('%] ');
    print colored("Compiling D source $i\n", 'green');
    `dmd -g -c $i`;
    $index++;
}

print("[100%] ");
print colored("Linking => ./umbra\n", 'magenta');

($stdout, $stderr, $exit) = capture {
    system("dmd -g *.o -of=umbra");
};

my @errors = split(/\n/, $stderr);

if ($exit != 0)
{
    print('(ld): ');
    print colored("Linker error!\n", 'red');
    foreach my $i (0..3)
    {
        print($errors[$i] . "\n");
    }
}
`rm *.o`;
