import Type;

class Module : Type
{
    private string moduleName;
    private Type attachedType;

    this(string t_moduleName, Type t_type)
    {
        super("Module");
        this.moduleName = t_moduleName;
        this.attachedType = t_type;
    }

    public string getModuleName()
    {
        return (this.moduleName);
    }

    public Type getAttachedType()
    {
        return (this.attachedType);
    }
}
