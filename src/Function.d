import Type;

class Function : Type
{
    private string funcName;
    private Type parameterType;
    private void delegate(Type*) funcPointer;
    private Type* parameter;

    this(string t_funcName, Type t_parameterType,
            void delegate(Type*) t_funcPointer, Type* t_parameter)
    {
        super("Function");
        this.funcName = t_funcName;
        this.parameterType = t_parameterType;
        this.funcPointer = t_funcPointer;
        this.parameter = t_parameter;
    }

    public string getFuncName()
    {
        return (this.funcName);
    }

    public Type getParameterType()
    {
        return (this.parameterType);
    }

    public void callFunction()
    {
        this.funcPointer(this.parameter);
    }
}
