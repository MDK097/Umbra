import Type;

class Text : Type
{
    private string font;
    private uint size;
    private string color;
    private string buffer;

    this(string t_buffer)
    {
        super("Text");
        this.buffer = t_buffer;
    }

    public string getFont()
    {
        return (this.font);
    }

    public uint getSize()
    {
        return (this.size);
    }

    public string getColor()
    {
        return (this.color);
    }

    public string getText()
    {
        return (this.buffer);
    }
}
