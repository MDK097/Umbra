# Umbra

![GitHub top language](https://img.shields.io/github/languages/top/mdk97/Umbra?style=for-the-badge)
![GitHub commit activity](https://img.shields.io/github/commit-activity/m/mdk97/Umbra?style=for-the-badge)
![GitHub last commit (branch)](https://img.shields.io/github/last-commit/mdk97/Umbra/master?style=for-the-badge)

Umbra is a Domain Specific Language for designing and prototyping cards for card games.

```Python
def Title : Text 
{
    Font("Noto Sans");
    Size(12);
    Color("Black");
    Value("Card Title");
}

def Art : Image 
{
    Resolution("800x600");
    File("image.png");
    Opacity(0.9);
}

def Description : Text 
{
    Font("Noto Serif");
    Size(10);
    Color("Black");
    Text("Card Description");
}

Card 
{
    this.Height():cm(7);
    this.Width():cm(4);

    Title.Above(Art):cm(0.5);
    Art.Above(Description):cm(0.3);
    Description.MarginBottom():mm(2);
}
```
